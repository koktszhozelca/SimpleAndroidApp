package com.zktechproduction.simpleandroidapp;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    /*
        TAG: used to indicate the Activity / Fragment that where is the log created.
     */
    String TAG = "[MainActivity]";

    /*
        For UI components, better use lazy initialization

        The word "TextView" is red, since the library for it is not imported.
        Import the library simply press Alt + Enter when the message is shown (blue).

        After imported the library, expand the 'import...' at the above.
        You will see 'import android.widget.TextView;' is added.

        Same situation for Button.
    */
    private TextView txtShow;
    private Button btnClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
            Init the UI components

            ViewClass objectName = (ViewClass) findViewById(R.id.ID);

            The simplest way to init a component is findViewById,
            you will see some different ways to do it later.

            e.g. Use Fragment

            Please note that the 'R.id.txtShow' is defined explicitly at activity_main.xml.

            To verify, please click on the components in activity_main.xml and looking for 'ID' in properties.

            activity_main.xml is located at app/res/layout/

            To change the app icon, you can replace the pictures in folder mipmap.

            The following website can help you to generate the required files.
            https://goo.gl/fO3xzT
         */
        txtShow = (TextView) findViewById(R.id.txtShow);
        btnClick = (Button) findViewById(R.id.btnClick);


        /*
            Here is one way to use the onClickListener for a Button
         */
        btnClick.setOnClickListener(new myOnClickListener());

        /*
            Shortcut key to comment and uncomment: Ctrl + /

            You can also try this by comment the above 'btnClick.setOnClickListener(new myOnClickListener());'
             and uncomment below code.
         */
//        btnClick.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//             /*
//                Create a log message explicitly, which will be shown in Android Monitor.
//
//                Shortcut key to open the Android Monitor: Alt + 6
//
//                Log.d(TAG, "Message");
//             */
//                Log.d(TAG, "The button is clicked");
//
//
//            /*
//                Change the text
//             */
//                txtShow.setText("Oh, you've clicked the button below");
//
//            /*
//                Create an alert dialog
//             */
//                new AlertDialog.Builder(MainActivity.this)
//                        .setTitle("Simple Android application tutorial")
//                        .setMessage("This tutorial is finished, please try to build it from scratch, which will help you more.")
//                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        }).create().show();
//            }
//        });

    }

    private class myOnClickListener implements Button.OnClickListener {
        @Override
        public void onClick(View v) {
            /*
                Create a log message explicitly, which will be shown in Android Monitor.

                Shortcut key to open the Android Monitor: Alt + 6

                Log.d(TAG, "Message");
             */
            Log.d(TAG, "The button is clicked");


            /*
                Change the text
             */
            txtShow.setText("Oh, you've clicked the button below");

            /*
                Create an alert dialog
             */
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Simple Android application tutorial")
                    .setMessage("This tutorial is finished, please try to build it from scratch, which will help you more.")
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
        }
    }
}
